import { flatten } from 'lodash';
import { User, Role, Permission } from '../models';

/**
* hasPermission middleware
* Check if user has specific or bunch of permissions
* @param {Array<String> | String} _data permission or array of permissions
* @return {Async Function} Koa Middleware
*/
export const hasPermission = _data => async (ctx, next) => {
  const alias = [].concat(_data);
  try {
    const user = await User.findById(ctx.state.user.id);
    if (!user) {
      ctx.throw(404, ctx.i18n.__("User doesn't exist"));
    }
    const roles = await Role.find({ _id: { $in: user.roles } });
    const permissions = (await Permission.find({
      _id: { $in: flatten(roles.map(role => role.permissions)) },
    })).map(permission => permission.slug);
    alias.forEach((permission) => {
      if (!permissions.includes(permission)) {
        ctx.throw(403);
      }
    });
    await next();
  } catch (e) {
    ctx.throw(400, e);
  }
};

/**
* hasRole middleware
* Check if user has specific or bunch of roles
* @param {Array<String> | String} _role role or array of roles
* @return {Async Function} Koa Middleware
*/
export const hasRole = _data => async (ctx, next) => {
  const alias = [].concat(_data);
  try {
    const user = await User.findById(ctx.state.user.id);
    if (!user) {
      ctx.throw(404, { detail: ctx.i18n.__("User doesn't exist") });
    }
    const roles = (await Role.find({ _id: { $in: user.roles } })).map(role => role.slug);
    alias.forEach((role) => {
      if (!roles.includes(role)) {
        ctx.throw(403);
      }
    });
    await next();
  } catch (e) {
    ctx.throw(500, e);
  }
};
