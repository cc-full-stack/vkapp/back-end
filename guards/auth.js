import jwt from 'koa-jwt';

/* eslint-disable import/prefer-default-export */
export const auth = jwt({ secret: process.env.JWT_KEY });
