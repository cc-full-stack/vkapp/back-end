# Back-End

> A Koa.js project

## Run dev server

```bash
# install dependencies
yarn install

# serve with hot reload at localhost:8080
yarn dev
```

## Build

```bash
# install dependencies
yarn install

# serve build 
yarn build && yarn start
```

## Generate documentation

```bash
# Generate to path ./apidoc without private routes
yarn docs

# Generate to path ./apidoc with private routes
yarn docs --private true
```
