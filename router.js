import pathToRegexp from 'path-to-regexp';
import methods from 'methods';
import mount from 'koa-mount';
import Koa from 'koa';

module.exports = class Router {
  constructor() {
    // Internally, our router will just use a Koa application as a middleware stack
    this.app = new Koa();
    methods.forEach((method) => {
      // Attach HTTP methods (e.g. get, post, put, delete) as router's intance methods
      this[method] = this.createRoute(method);
    });
    // Alias for keyword `delete`
    this.del = this.delete;
    this.all = this.createRoute();
  }

  /**
   * A route factory, used to populate router methods.
   */
  /* eslint-disable class-methods-use-this */
  createRoute(method) {
    return function h(path, handler) {
      // Each route is basically a middleware that performs matching on req.method and pathname
      this.app.use(async (ctx, next) => {
        if (!Router.matchMethod(ctx, method)) {
          return next();
        }
        return Router.createPathHandler(path, handler)(ctx, next);
      });
    };
  }
  /* eslint-enable class-methods-use-this */

  /**
   * Prefix handler, similar to Express's `.use`.
   */
  use(prefix, handler) {
    if (typeof prefix === 'string') {
      this.app.use(Router.createPathHandler(prefix, handler, { end: false }));
    } else {
      /* eslint-disable no-param-reassign */
      handler = prefix;
      /* eslint-enable no-param-reassign */
      this.app.use(handler);
    }
  }

  /**
   * Export router as Koa middleware.
   */
  routes() {
    return mount(this.app);
  }

  /**
   * Mount another `router` with optional `prefix`.
   */

  mount(prefix, router) {
    if (typeof prefix === 'string') {
      this.app.use(mount(prefix, Router.convertRouter(router)));
    } else {
      this.app.use(mount(Router.convertRouter(prefix)));
    }
  }

  static convertRouter(handler) {
    if (!handler) {
      throw new Error('Handler should be defined.');
    }
    if (typeof handler.routes === 'function') {
      return handler.routes();
    }
    if (typeof handler === 'function') {
      return handler;
    }
    throw new Error(`Unsupported handler type: ${typeof handler === 'function'}`);
  }

  static createPathHandler(path, handler, options) {
    /* eslint-disable no-param-reassign */
    options = options || {};
    /* eslint-enable no-param-reassign */
    const keys = [];
    const re = pathToRegexp(path, keys, options);
    return async (ctx, next) => {
      const m = re.exec(ctx.path);
      if (m) {
        const args = m.slice(1).map(Router.decode);
        ctx.params = {};
        args.forEach((arg, i) => {
          ctx.params[i] = arg;
        });
        keys.forEach((key, i) => {
          ctx.params[key.name] = args[i];
        });
        /* eslint-disable no-return-await */
        return await handler(ctx, next);
        /* eslint-enable no-return-await */
      }
      return next();
    };
  }

  static decode(val) {
    return val ? decodeURIComponent(val) : null;
  }

  static matchMethod(ctx, _method) {
    if (!_method) {
      return true;
    }
    const method = _method.toUpperCase();
    return ctx.method === method || (method === 'GET' && ctx.method === 'HEAD') || false;
  }
};
