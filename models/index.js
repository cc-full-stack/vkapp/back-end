import mongoose from 'mongoose';
import { DB_URL } from '../config';
import userSchema from './user';
import userTypeSchema from './user-type';
import roleSchema from './role';
import permissionSchema from './permission';

mongoose.Promise = global.Promise;
mongoose.connect(DB_URL, {
  useMongoClient: true,
});

/* eslint-disable no-console */
mongoose.connection.on('error', (e) => {
  console.log(`Error occured on database connection: ${e}`);
});
mongoose.connection.once('open', () => {
  console.log(`Connected to database: ${DB_URL}`);
});
/* eslint-enable no-console */

export const User = mongoose.model('Users', userSchema);
export const UserType = mongoose.model('Users.Types', userTypeSchema);
export const Role = mongoose.model('Roles', roleSchema);
export const Permission = mongoose.model('Permissions', permissionSchema);
