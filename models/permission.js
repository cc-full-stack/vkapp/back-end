import { Schema } from 'mongoose';

const permissionSchema = new Schema({
  slug: { type: String, required: true, unique: true },
  description: { type: String },
});

export default permissionSchema;
