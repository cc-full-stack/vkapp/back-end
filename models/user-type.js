import { Schema } from 'mongoose';

const userTypeSchema = new Schema({
  name: { type: String, required: true, unique: true },
});

export default userTypeSchema;
