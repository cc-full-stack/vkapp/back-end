import { Schema } from 'mongoose';
import bcrypt from 'bcrypt';
import validate from 'mongoose-validator';

const passwordValidator = [
  validate({
    validator: 'matches',
    arguments: /^(?=.*?[A-Z]).{1,}$/,
    message: 'Password needs to have at least one uppercase character.',
  }),
  validate({
    validator: 'matches',
    arguments: /^(?=.*?[a-z]).{1,}$/,
    message: 'Password needs to have at least one lower case character.',
  }),
  validate({
    validator: 'matches',
    arguments: /^(?=.*?[\d]).{1,}$/,
    message: 'Password needs to have at least one number.',
  }),
  validate({
    validator: 'matches',
    arguments: /^(?=.*?[\W]).{1,}$/,
    message: 'Password needs to have at least one special character.',
  }),
  validate({
    validator: 'isLength',
    arguments: [8, 35],
    message: 'Password should be between {ARGS[0]} and {ARGS[1]} characters',
  }),
];

const usernameValidator = [
  validate({
    validator: 'isLength',
    arguments: [3, 25],
    message: 'Username should be between {ARGS[0]} and {ARGS[1]} characters',
  }),
  validate({
    validator: 'isAlphanumeric',
    message: 'Username must contain letters and numbers only',
  }),
];

const nameValidator = [
  validate({
    validator: 'matches',
    arguments: /^([a-zA-Z]{3,20})+$/,
  }),
];

const userSchema = new Schema({
  firstname: { type: String, required: true, validate: nameValidator },
  lastname: { type: String, required: true, validate: nameValidator },
  username: {
    type: String,
    lowercase: true,
    required: true,
    unique: true,
    validate: usernameValidator,
  },
  password: {
    type: String,
    required: true,
    validate: passwordValidator,
    select: false,
  },
  userTypeId: { type: Schema.Types.ObjectId, default: null },
  roles: { type: [Schema.Types.ObjectId] },
});

// const presave =

userSchema.pre('save', function h(next) {
  const user = this;
  if (user.isModified('password')) {
    try {
      user.password = bcrypt.hashSync(
        user.password,
        bcrypt.genSaltSync(parseInt(process.env.SALT_ROUNDS, 10)),
      );
      next();
    } catch (e) {
      next(e);
    }
  }
  return next();
});

userSchema.methods.comparePasswords = function comparePasswords(password) {
  return bcrypt.compareSync(password, this.password);
};

export default userSchema;
