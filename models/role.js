import { Schema } from 'mongoose';

const roleSchema = new Schema({
  slug: { type: String, required: true, unique: true },
  description: { type: String },
  permissions: { type: [Schema.Types.ObjectId] },
});

export default roleSchema;
