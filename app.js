import {} from 'dotenv/config';
import Koa from 'koa';
import cors from 'kcors';
import bodyParser from 'koa-bodyparser';
import locale from 'koa-locale';
import i18n from 'koa-i18n';
import { PORT } from './config';

const app = new Koa();

locale(app);

app.use(i18n(app, {
  directory: 'locales',
  locales: ['en', 'ro'],
  modes: ['query', 'subdomain', 'cookie', 'header', 'url', 'tld'],
  extension: '.json',
}));

app.use(cors());
app.use(bodyParser());

app.use((ctx, next) =>
  next().catch((err) => {
    if (err.status === 401) {
      ctx.status = err.status;
      ctx.body = err.message;
    } else {
      throw err;
    }
  }));

app.listen(PORT);
/* eslint-disable no-console */
console.log(`App listening on port: ${PORT}`);
/* eslint-enable no-console */

export default app;
