import app from './app';
import baseController from './controllers';

app.use(baseController.routes());
