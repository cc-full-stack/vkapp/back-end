export const { DB_PROTOCOL } = process.env;
export const { DB_HOST } = process.env;
export const { DB_NAME } = process.env;
export const DB_URL = `${DB_PROTOCOL}://${DB_HOST}/${DB_NAME}`;
