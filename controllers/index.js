import Router from '../router';
import user from './user';
import userType from './user-type';
import role from './role';
import permission from './permission';

const baseController = new Router();

baseController.mount('/users/types', userType);
baseController.mount('/users', user);
baseController.mount('/roles', role);
baseController.mount('/permissions', permission);

export default baseController;
