import { Role, Permission } from '../models';
import { auth, hasRole } from '../guards';
import Router from '../router';

const router = new Router();

/**
 * @apiDefine ErrorNotFound
 *
 * @apiError NotFound Role not found for specified id.
 *
 * @apiErrorExample {json} 404 Error example
 * HTTP/1.1 404 Not Found
 * Role doesn't exist
 */

/**
 * @apiDefine ErrorMissingAuth
 *
 * @apiError Unathorized Access denied due to missing auth token.
 *
 * @apiErrorExample {json} 401 Error example
 * HTTP/1.1 401 Unathorized
 * Authentication Error
 */

/**
 * @apiDefine ErrorInsufficientPermissions
 *
 * @apiError Forbidden Access denied due to insufficient permissions.
 *
 * @apiErrorExample {json} 403 Error example
 * HTTP/1.1 403 Forbidden
 * Forbidden
 */

/**
 * @apiDefine ErrorBadRequest
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 * Roles validation failed: slug: Path `slug` is required.
 */

/**
 * @apiDefine ErrorNotAcceptable
 *
 * @apiError NotAcceptable Provided input contain redundant data.
 *
 * @apiErrorExample {json} 406 Error example
 * HTTP/1.1 406 Not Acceptable
 * Role allready exists
 */

/**
 * @apiDefine AuthHeader
 *
 * @apiHeader {String} Authorization Auth token given on login or register.
 *
 * @apiHeaderExample {json} Authorization Header-Example:
 *    {
 *      "Authorization": "token string"
 *    }
 */

/**
 * @apiDefine RoleRequestExample
 *
 * @apiExample {json} Example request:
 *    {
 *      "slug": "Admin",
 *      "description": "Main role (Highest level)"
 *    }
 */

/**
 * @apiDefine PermissionRequestExample
 *
 * @apiExample {json} Example request:
 *    [
 *      "59d637c42c4b825d8c683711",
 *      "59d637c42c4b825d8c683712"
 *    ]
 */

/**
 * @apiDefine SuccessResponseExampleCreate
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 201 Created
 *    {
 *      "slug": "Admin",
 *      "description": "Main role (Highest level)",
 *      "permissions": []
 *    }
 */

/**
 * @apiDefine SuccessResponseExampleOK
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *    {
 *      "slug": "Admin",
 *      "description": "Main role (Highest level)",
 *      "permissions": []
 *    }
 */

/**
 * @apiDefine PermissionRequestStructure
 *
 * @apiParam (Request body) {String[]} permissions Array of ids related to permissions
 */

/**
 * @apiDefine RequestStructure
 *
 * @apiParam (Request body) {Object} role Role's object
 * @apiParam (Request body) {String} role.slug Identifier for role
 * @apiParam (Request body) {String} [role.description] Description for role
 */

/**
 * @apiDefine ResponseStructure
 *
 * @apiSuccess (Response body) {Object} role Role's object
 * @apiSuccess (Response body) {String} role.slug Identifier for role
 * @apiSuccess (Response body) {String} [role.description] Description for role
 * @apiSuccess (Response body) {String[]} role.permissions Array of ids specific for permissions
 */

router.use(auth);

/**
 * @api {get} /roles List all roles
 * @apiName GetRoles
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiSuccess {Object[]} roles Role's list
 * @apiSuccess {String} roles.slug Role's unique identifier
 * @apiSuccess {String} [roles.description] Role's description
 * @apiSuccess {String[]} roles.permissions Array of role's permissions ids
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * [
 *    {
 *      "slug": "Admin",
 *      "description": "Main role (Highest level)",
 *      "permissions": []
 *    }
 * ]
 */
router.get('/', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const roles = await Role.find({});
		if (!roles) {
			ctx.throw(404);
		}
		ctx.status = 200;
		ctx.body = roles;
	} catch (e) {
		ctx.throw(500, e);
	}
});

/* -------------------- Only Admin can perform this actions ----------------- */
router.use(hasRole('Admin'));

/**
 * @api {post} /roles Create new role
 * @apiName CreateRole
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiUse AuthHeader
 *
 * @apiUse RoleRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 * @apiUse ErrorNotAcceptable
 *
 * @apiUse RequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleCreate
 */
router.post('/', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const role = new Role(ctx.body);
		await role.save();
		ctx.status = 201;
	} catch (e) {
		if (e.name === 'MongoError') {
			ctx.throw(406, ctx.i18n.__('Role allready exists'));
		}
		ctx.throw(400, e);
	}
});

/**
 * @api {put} /roles/:id Change role's properties
 * @apiName ChangeRole
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Role's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse RoleRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 *
 * @apiUse RequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.put('/:id', async ctx => {
	ctx.body = ctx.request.body;
	try {
		delete ctx.body.permissions;
		await Role.findByIdAndUpdate(ctx.params.id, ctx.body, {
			new: true,
			overwrite: true,
			runValidators: true,
		});
		ctx.status = 200;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {patch} /roles/:id Update role's properties
 * @apiName UpdateRole
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Role's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse RoleRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 *
 * @apiUse RequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.patch('/:id', async ctx => {
	ctx.body = ctx.request.body;
	try {
		delete ctx.body.permissions;
		await Role.findByIdAndUpdate(ctx.params.id, ctx.body, {
			runValidators: true,
		});
		ctx.body = await Role.findById(ctx.params.id);
		ctx.status = 200;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {post} /roles/:id/permissions Attaches permissions to role
 * @apiName AttachRolePermissions
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Role's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse PermissionRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 * @apiUse ErrorNotFound
 *
 * @apiUse PermissionRequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.post('/:id/permissions', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const role = await Role.findById(ctx.params.id);
		if (!role) {
			ctx.throw(404, ctx.i18n.__("Role doesn't exist"));
		}
		const permissions = await Permission.find({ _id: { $in: ctx.body } });
		permissions.forEach(permission => {
			role.permissions.push(permission);
		});
		await role.save();
		ctx.status = 200;
		ctx.body = role;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {put} /roles/:id/permissions Sets permissions for role
 * @apiName SetRolePermissions
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Role's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse PermissionRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 * @apiUse ErrorNotFound
 *
 * @apiUse PermissionRequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.put('/:id/permissions', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const role = await Role.findById(ctx.params.id);
		if (!role) {
			ctx.throw(404, ctx.i18n.__("Role doesn't exist"));
		}
		role.permissions = await Permission.find({ _id: { $in: ctx.body } });
		await role.save();
		ctx.status = 200;
		ctx.body = role;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {delete} /roles/:id/permissions Detaches permissions from role
 * @apiName DetachRolePermissions
 * @apiGroup Roles
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Role's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse PermissionRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 * @apiUse ErrorNotFound
 *
 * @apiUse PermissionRequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.delete('/:id/permissions', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const role = await Role.findById(ctx.params.id);
		if (!role) {
			ctx.throw(404, ctx.i18n.__("Role doesn't exist"));
		}
		const permissions = await Permission.find({ _id: { $in: ctx.body } });
		permissions.forEach(permission => {
			role.permissions = role.permissions.filter(
				rpermission => rpermission.toString() !== permission._id.toString()
			);
		});
		await role.save();
		ctx.status = 200;
		ctx.body = role;
	} catch (e) {
		ctx.throw(400, e);
	}
});

export default router;
