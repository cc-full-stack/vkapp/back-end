import { Permission } from '../models';
import { auth, hasRole } from '../guards';
import Router from '../router';

const router = new Router();

/**
 * @apiDefine ErrorMissingAuth
 *
 * @apiError Unathorized Access denied due to missing auth token.
 *
 * @apiErrorExample {json} 401 Error example
 * HTTP/1.1 401 Unathorized
 * Authentication Error
 */

/**
 * @apiDefine ErrorInsufficientPermissions
 *
 * @apiError Forbidden Access denied due to insufficient permissions.
 *
 * @apiErrorExample {json} 403 Error example
 * HTTP/1.1 403 Forbidden
 * Forbidden
 */

/**
 * @apiDefine ErrorBadRequest
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 * Permissions validation failed: slug: Path `slug` is required.
 */

/**
 * @apiDefine ErrorNotAcceptable
 *
 * @apiError NotAcceptable Provided input contain redundant data.
 *
 * @apiErrorExample {json} 406 Error example
 * HTTP/1.1 406 Not Acceptable
 * Permission allready exists
 */

/**
 * @apiDefine AuthHeader
 *
 * @apiHeader {String} Authorization Auth token given on login or register.
 *
 * @apiHeaderExample {json} Authorization Header-Example:
 *    {
 *      "Authorization": "token string"
 *    }
 */

/**
 * @apiDefine RequestExample
 *
 * @apiExample {json} Example request:
 *    {
 *      "slug": "add_user",
 *      "description": "Create user"
 *    }
 */

/**
 * @apiDefine SuccessResponseExampleCreate
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 201 Created
 *    {
 *      "slug": "add_user",
 *      "description": "Create user"
 *    }
 */

/**
 * @apiDefine SuccessResponseExampleOK
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *    {
 *      "slug": "add_user",
 *      "description": "Create user"
 *    }
 */

/**
 * @apiDefine RequestStructure
 *
 * @apiParam (Request body) {Object} permission Permission's object
 * @apiParam (Request body) {String} permission.slug Identifier for permission
 * @apiParam (Request body) {String} [permission.description] Description for permission
 */

/**
 * @apiDefine ResponseStructure
 *
 * @apiSuccess (Response body) {Object} permission Permission's object
 * @apiSuccess (Response body) {String} permission.slug Identifier for permission
 * @apiSuccess (Response body) {String} [permission.description] Description for permission
 */

router.use(auth);

/**
 * @api {get} /permissions List all permissions
 * @apiName GetPermissions
 * @apiGroup Permissions
 *
 * @apiPrivate
 *
 * @apiSuccess {Object[]} permissions Permission's list
 * @apiSuccess {String} permissions.slug Permission's unique identifier
 * @apiSuccess {String} [permissions.description] Permission's description
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * [
 *    {
 *      "slug": "add_user",
 *      "description": "Create user"
 *    }
 * ]
 */
router.get('/', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const permissions = await Permission.find({});
		if (!permissions) {
			ctx.throw(404);
		}
		ctx.status = 200;
		ctx.body = permissions;
	} catch (e) {
		ctx.throw(500, e);
	}
});

/* -------------------- Only Admin can perform this actions ----------------- */
router.use(hasRole('Admin'));

/**
 * @api {post} /permissions Create new permission
 * @apiName CreatePermission
 * @apiGroup Permissions
 *
 * @apiPrivate
 *
 * @apiUse AuthHeader
 *
 * @apiUse RequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 * @apiUse ErrorNotAcceptable
 *
 * @apiUse RequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleCreate
 */
router.post('/', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const permission = new Permission(ctx.body);
		await permission.save();
		ctx.status = 201;
	} catch (e) {
		if (e.name === 'MongoError') {
			ctx.throw(406, ctx.i18n.__('Permission allready exists'));
		}
		ctx.throw(400, e);
	}
});

/**
 * @api {put} /permissions/:id Change permission
 * @apiName ChangePermission
 * @apiGroup Permissions
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Permission's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse RequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 *
 * @apiUse RequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.put('/:id', async ctx => {
	ctx.body = ctx.request.body;
	try {
		await Permission.findByIdAndUpdate(ctx.params.id, ctx.body, {
			new: true,
			overwrite: true,
			runValidators: true,
		});
		ctx.status = 200;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {patch} /permissions/:id Update permission
 * @apiName UpdatePermission
 * @apiGroup Permissions
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Permission's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse RequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 *
 * @apiUse RequestStructure
 * @apiUse ResponseStructure
 *
 * @apiUse SuccessResponseExampleOK
 */
router.patch('/:id', async ctx => {
	ctx.body = ctx.request.body;
	try {
		await Permission.findByIdAndUpdate(ctx.params.id, ctx.body, {
			runValidators: true,
		});
		ctx.body = await Permission.findById(ctx.params.id);
		ctx.status = 200;
	} catch (e) {
		ctx.throw(400, e);
	}
});

export default router;
