import { UserType } from '../models';
import { auth, hasPermission } from '../guards';
import Router from '../router';

const router = new Router();

/**
 * @apiDefine ErrorMissingAuth
 * @apiError Unathorized Access denied due to missing auth token.
 * @apiErrorExample {json} 401 Error example
 * HTTP/1.1 401 Unathorized
 * Authentication Error
 */

/**
 * @apiDefine ErrorInsufficientPermissions
 *
 * @apiError Forbidden Access denied due to insufficient permissions.
 *
 * @apiErrorExample {json} 403 Error example
 * HTTP/1.1 403 Forbidden
 * Forbidden
 */

/**
 * @apiDefine ErrorBadRequest
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 * Roles validation failed: slug: Path `slug` is required.
 */

/**
 * @apiDefine ErrorNotAcceptable
 *
 * @apiError NotAcceptable Provided input contain redundant data.
 *
 * @apiErrorExample {json} 406 Error example
 * HTTP/1.1 406 Not Acceptable
 * User type allready exists
 */

/**
 * @apiDefine AuthHeader
 *
 * @apiHeader {String} Authorization Auth token given on login or register.
 *
 * @apiHeaderExample {json} Authorization Header-Example:
 *    {
 *      "Authorization": "token string"
 *    }
 */

/**
 * @apiDefine UserTypeRequestExample
 *
 * @apiExample {json} Example request:
 *    {
 *      "name": "Staff"
 *    }
 */

/**
 * @apiDefine UserTypeSuccessResponseExampleCreate
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 201 Created
 *    {
 *      "slug": "Admin",
 *      "description": "Main role (Highest level)",
 *      "permissions": []
 *    }
 */

/**
 * @apiDefine UserTypeRequestStructure
 *
 * @apiParam (Request body) {Object} type UserType's object
 * @apiParam (Request body) {String} type.name Identifier for UserType
 */

/**
 * @apiDefine UserTypeResponseStructure
 *
 * @apiParam (Response body) {Object} type UserType's object
 * @apiParam (Response body) {String} type.name Identifier for UserType
 */

/**
 * @api {get} /users/types List all user types
 * @apiName GetUserTypes
 * @apiGroup Users
 *
 * @apiSuccess {Object[]} types UserType's list
 * @apiSuccess {String} types.name UserType's unique name
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 * [
 *    {
 *      "name": "Staff"
 *    }
 * ]
 */
router.get('/', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const userTypes = await UserType.find({});
		if (!userTypes) {
			ctx.throw(404, ctx.i18n.__('No user types'));
		}
		ctx.status = 200;
		ctx.body = userTypes;
	} catch (e) {
		ctx.throw(500, e);
	}
});

router.use(auth);
router.use(hasPermission('create_user_type'));

/**
 * @api {post} /users/types Create new user type
 * @apiName CreateUserType
 * @apiGroup Users
 *
 * @apiPrivate
 *
 * @apiUse AuthHeader
 *
 * @apiUse UserTypeRequestExample
 *
 * @apiUse ErrorMissingAuth
 * @apiUse ErrorInsufficientPermissions
 * @apiUse ErrorBadRequest
 * @apiUse ErrorNotAcceptable
 *
 * @apiUse UserTypeRequestStructure
 * @apiUse UserTypeResponseStructure
 *
 * @apiUse UserTypeSuccessResponseExampleCreate
 */
router.post('/', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const userType = new UserType(ctx.body);
		await userType.save();
		ctx.status = 201;
	} catch (e) {
		if (e.name === 'MongoError') {
			ctx.throw(406, ctx.i18n.__('User type allready exists'));
		}
		ctx.throw(400, e);
	}
});

export default router;
