import { sign } from 'jsonwebtoken';
import Router from '../router';
import { auth, hasRole } from '../guards';
import { User, Role } from '../models';

const router = new Router();

/**
 * @apiDefine AuthHeader
 *
 * @apiHeader {String} Authorization Auth token given on login or register.
 *
 * @apiHeaderExample {json} Authorization Header-Example:
 *    {
 *      "Authorization": "token string"
 *    }
 */

/**
 * @apiDefine ErrorNotFound
 *
 * @apiError NotFound User not found for specified id.
 *
 * @apiErrorExample {json} 404 Error example
 * HTTP/1.1 404 Not Found
 * User doesn't exist
 */

/**
 * @apiDefine ErrorMissingAuth
 *
 * @apiError Unathorized Access denied due to missing auth token or provided is expired.
 *
 * @apiErrorExample {json} 401 Error example
 * HTTP/1.1 401 Unathorized
 * Authentication Error
 */

/**
 * @apiDefine ResponseBody
 *
 * @apiSuccess (Response body) {Object} user User data
 * @apiSuccess (Response body) {String} user.username User's identifier
 * @apiSuccess (Response body) {String} user.lastname User's lastname
 * @apiSuccess (Response body) {String} user.firstname User's firstname
 * @apiSuccess (Response body) {Array} user.roles User's roles
 * @apiSuccess (Response body) {String} user.userTypeId User type's id
 */

/**
 * @apiDefine ResponseExample
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *    {
 *    	"_id": "5a4690d157a6e51aee4e4388",
 *    	"firstname": "Cuciurca",
 *    	"lastname": "Constantin",
 *    	"username": "ccuciurca",
 *    	"roles": [
 *    		"5a4b68a090579d434cc5456c"
 *    	],
 *    	"userTypeId": "5a469025fd53778cdea05050"
 *    }
 */

/**
 * @apiDefine UserRoleRequestStructure
 *
 * @apiParam (Request body) {String[]} roles Array of ids related to roles
 */

/**
 * @apiDefine UserRoleRequestExample
 *
 * @apiExample {json} Example request:
 *    [
 *      "59d637c42c4b825d8c683711",
 *      "59d637c42c4b825d8c683712"
 *    ]
 */

/**
 * @api {post} /users/login Authenticate user
 * @apiName Authenticate
 * @apiGroup Users
 *
 * @apiExample {json} Example request:
 *    {
 *      "username": "ccuciurca",
 *      "password": "VerryStrongPassword"
 *    }
 *
 * @apiError NotFound User not found for specified username.
 * @apiErrorExample {json} 404 Error example
 * HTTP/1.1 404 Not Found
 * User doesn't exist
 *
 * @apiError BadRequest Bad auth credentials.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 * Bad credentials
 *
 * @apiParam (Request body) {Object} credentials Auth credentials object
 * @apiParam (Request body) {String} credentials.username User's identifier
 * @apiParam (Request body) {String} credentials.password User's password
 * @apiSuccess (Response body) {Object} data Authentication data
 * @apiSuccess (Response body) {String} data.token User's JSON Web Token to use on authenticated routes
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *    {
 *      "token": "Bearer eyJhbGciOi..."
 *    }
 */
router.post('/login', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const user = await User.findOne({ username: ctx.body.username }).select(
			'+password'
		);
		if (!user) {
			ctx.throw(404, ctx.i18n.__("User doesn't exist"));
		}
		if (!user.comparePasswords(ctx.body.password)) {
			ctx.throw(400, ctx.i18n.__('Bad credentials'));
		}
		ctx.status = 200;
		ctx.body = {
			token: `${process.env.JWT_SCHEMA} ${sign(
				{ id: user._id },
				process.env.JWT_KEY
			)}`,
		};
	} catch (e) {
		ctx.throw(400, ctx.i18n.__('Bad credentials'));
	}
});

/**
 * @api {post} /users/register Register new user
 * @apiName Register
 * @apiGroup Users
 *
 * @apiExample {json} Example request:
 *    {
 *      "username": "ccuciurca",
 *      "firstname": "Cuciurca",
 *      "lastname": "Constantin",
 *      "password": "VerryStrongPassword"
 *    }
 *
 * @apiError NotAcceptable User with specified username already exists.
 * @apiErrorExample {json} 406 Not Acceptable
 * HTTP/1.1 406 Not Found
 * Username allready exists
 *
 * @apiError BadRequest Bad auth credentials.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 * Users validation failed: password: Password needs to have at least one number.
 *
 * @apiParam (Request body) {Object} user User data
 * @apiParam (Request body) {String} user.username User's identifier
 * @apiParam (Request body) {String} user.lastname User's lastname
 * @apiParam (Request body) {String} user.firstname User's firstname
 * @apiParam (Request body) {String} user.password User's password
 * @apiSuccess (Response body) {Object} data Authentication data
 * @apiSuccess (Response body) {String} data.token User's JSON Web Token to use on authenticated routes
 *
 * @apiSuccessExample {json} Success-Response:
 * HTTP/1.1 200 OK
 *    {
 *      "token": "Bearer eyJhbGciOi..."
 *    }
 */
router.post('/register', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const user = new User(ctx.body);
		await user.save();
		ctx.status = 201;
		ctx.body = {
			token: `${process.env.JWT_SCHEMA} ${sign(
				{ id: user._id },
				process.env.JWT_KEY
			)}`,
		};
	} catch (e) {
		if (e.name === 'MongoError') {
			ctx.throw(406, ctx.i18n.__('Username allready exists'));
		}
		ctx.throw(400, e);
	}
});

router.use(auth);

/**
 * @api {get} /users/me Get authenticated user
 * @apiName GetAuthUser
 * @apiGroup Users
 *
 * @apiPrivate
 *
 * @apiUse AuthHeader
 *
 * @apiError NotFound User not found for provided token.
 *
 * @apiErrorExample {json} 404 Error example
 * HTTP/1.1 404 Not Found
 * User doesn't exist
 *
 * @apiUse ErrorMissingAuth
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 *
 * @apiUse ResponseBody
 * @apiUse ResponseExample
 */
router.get('/me', async ctx => {
	try {
		const user = await User.findById(ctx.state.user.id);
		if (!user) {
			ctx.throw(404, ctx.i18n.__("User doesn't exist"));
		}
		ctx.status = 200;
		ctx.body = user;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {get} /users/:id Get details for specific user
 * @apiName GetUser
 * @apiGroup Users
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Users's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse ErrorNotFound
 * @apiUse ErrorMissingAuth
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 *
 * @apiUse ResponseBody
 * @apiUse ResponseExample
 */
router.get('/:id', async ctx => {
	try {
		const user = await User.findById(ctx.params.id);
		if (!user) {
			ctx.throw(404, ctx.i18n.__("User doesn't exist"));
		}
		ctx.status = 200;
		ctx.body = user;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/* -------------------- Only Admin can perform this actions ----------------- */
router.use(hasRole('Admin'));

/**
 * @api {post} /users/:id/roles Attaches roles to specific user
 * @apiName AttachUserRole
 * @apiGroup Users
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Users's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse ErrorNotFound
 * @apiUse ErrorMissingAuth
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 *
 * @apiUse UserRoleRequestStructure
 * @apiUse UserRoleRequestExample
 * @apiUse ResponseBody
 * @apiUse ResponseBody
 * @apiUse ResponseExample
 */
router.post('/:id/roles', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const user = await User.findById(ctx.params.id);
		if (!user) {
			ctx.throw(404, ctx.i18n.__("User doesn't exist"));
		}
		const roles = await Role.find({ _id: { $in: ctx.body } });
		const userRolesIds = user.roles.map(role => role.toString());
		roles.forEach(role => {
			if (!userRolesIds.includes(role._id.toString())) {
				user.roles.push(role);
			}
		});
		await user.save();
		ctx.status = 200;
		ctx.body = user;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {put} /users/:id/roles Set roles for specific user
 * @apiName SetUserRole
 * @apiGroup Users
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Users's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse ErrorNotFound
 * @apiUse ErrorMissingAuth
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 *
 * @apiUse UserRoleRequestStructure
 * @apiUse UserRoleRequestExample
 * @apiUse ResponseBody
 * @apiUse ResponseBody
 * @apiUse ResponseExample
 */
router.put('/:id/roles', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const user = await User.findById(ctx.params.id);
		if (!user) {
			ctx.throw(404, ctx.i18n.__("User doesn't exist"));
		}
		user.roles = await Role.find({ _id: { $in: ctx.body } });
		await user.save();
		ctx.status = 200;
		ctx.body = user;
	} catch (e) {
		ctx.throw(400, e);
	}
});

/**
 * @api {delete} /users/:id/roles Detaches roles from specific user
 * @apiName DetachUserRole
 * @apiGroup Users
 *
 * @apiPrivate
 *
 * @apiParam (Url params) {String} id Users's id
 *
 * @apiUse AuthHeader
 *
 * @apiUse ErrorNotFound
 * @apiUse ErrorMissingAuth
 *
 * @apiError BadRequest Provided input does't match required.
 *
 * @apiErrorExample {json} 400 Error example
 * HTTP/1.1 400 Bad Request
 *
 * @apiUse UserRoleRequestStructure
 * @apiUse UserRoleRequestExample
 * @apiUse ResponseBody
 * @apiUse ResponseBody
 * @apiUse ResponseExample
 */
router.delete('/:id/roles', async ctx => {
	ctx.body = ctx.request.body;
	try {
		const user = await User.findById(ctx.params.id);
		if (!user) {
			ctx.throw(404, ctx.i18n.__("User doesn't exist"));
		}
		let roles = await Role.find({ _id: { $in: ctx.body } });
		roles = roles.map(role => role._id.toString());
		user.roles = user.roles.filter(urole => !roles.includes(urole.toString()));
		await user.save();
		ctx.status = 200;
		ctx.body = user;
	} catch (e) {
		ctx.throw(400, e);
	}
});

export default router;
